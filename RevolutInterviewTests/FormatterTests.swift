//
//  FormatterTests.swift
//  RevolutInterviewTests
//
//  Created by Kerem Gunduz on 5.08.2018.
//

import XCTest
import Quick
import Nimble
@testable import RevolutInterview

class FormatterTests: BaseTest {

    override func spec() {
        describe("Formatter Class Tests") {
            context("Formatter formatDouble function tests") {
                var result: String = ""
                beforeEach {
                    result = ""
                }
                it("formatDouble with two floating point should return correct value") {
                    result = Formatter.formatDouble(124.45)
                    expect(result).to(equal("124.4500"))
                }
                it("formatDouble with six floating point should return correct value") {
                    result = Formatter.formatDouble(14.453234)
                    expect(result).to(equal("14.4532"))
                }
                it("formatDouble without floating point should return correct value") {
                    result = Formatter.formatDouble(1)
                    expect(result).to(equal("1.0000"))
                }
                it("formatDouble with zero should return correct value") {
                    result = Formatter.formatDouble(0)
                    expect(result).to(equal("0.0000"))
                }
            }
            context("Formatter parseString function tests") {
                var result: Double?
                beforeEach {
                    result = nil
                }
                it("parseString with nil should return 0") {
                    result = Formatter.parseString(nil)
                    expect(result).to(equal(0))
                }
                it("getCurrency with code method should return correnct object") {
                    result = Formatter.parseString("44")
                    expect(result).to(equal(Double(44)))
                }
                it("getCurrency with code method should return index") {
                    result = Formatter.parseString("653.34")
                    expect(result).to(equal(Double(653.34)))
                }
                it("getCurrency with code method should return correct index") {
                    result = Formatter.parseString("0")
                    expect(result).to(equal(Double(0)))
                }
            }
        }
    }
}
