//
//  BaseTest.swift
//  RevolutInterviewTests
//
//  Created by Kerem Gunduz on 5.08.2018.
//

import XCTest
import Quick
import Nimble
@testable import RevolutInterview

class BaseTest: QuickSpec {

}

func getMockList() -> [Currency] {
    return [
        Currency(code: "EUR", rate: 1),
        Currency(code: "USD", rate: 0.56),
        Currency(code: "RUB", rate: 3.6),
        Currency(code: "USD", rate: 0.56),
        Currency(code: "RUB", rate: 3.6),
        Currency(code: "ILS", rate: 2.5),
        Currency(code: "PHP", rate: 10.27),
        Currency(code: "CAD", rate: 0.45),
        Currency(code: "JPY", rate: 142.78),
        Currency(code: "GBP", rate: 0.3),
        Currency(code: "BRL", rate: 4)]
}
