//
//  CurrencyModelTests.swift
//  RevolutInterviewTests
//
//  Created by Kerem Gunduz on 5.08.2018.
//

import XCTest
import Quick
import Nimble
@testable import RevolutInterview

class CurrencyModelTests: BaseTest {

    override func spec() {
        describe("Currency Class Tests") {
            var list: [Currency] = [Currency]()
            beforeEach {
                list = getMockList()
            }

            context("CurrencyModel equality") {
                it("equal operator should return true for same codes") {
                    expect(list[0]).to(equal(list[0]))
                }
                it("equal operator should return false for different codes") {
                    expect(list[0]).toNot(equal(list[1]))
                }
            }
        }
    }
}
