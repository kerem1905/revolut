//
//  UnitHelperTests.swift
//  RevolutInterviewTests
//
//  Created by Kerem Gunduz on 5.08.2018.
//

import XCTest

import XCTest
import Quick
import Nimble
@testable import RevolutInterview

class UnitHelperTests: BaseTest {

    override func spec() {
        describe("UnitHelper Class Tests") {

            context("UnitHelper calculateAmount function tests") {
                var baseCurrency: Currency?
                var toCurrency: Currency?
                var amount: Double?
                beforeEach {
                    amount = nil
                    baseCurrency = Currency.init(code: "GBP", rate: 1)
                    toCurrency = Currency.init(code: "EUR", rate: 3)
                }

                it("calculateAmount with zero base rate should return zero") {
                    baseCurrency?.rate = 0
                    amount = UnitHelper.calculateAmount(baseCurrency: baseCurrency!, toCurrency: toCurrency!)
                    expect(amount).to(equal(0))
                }
                it("calculateAmount with non zero base rate should return correnct value") {
                    amount = UnitHelper.calculateAmount(baseCurrency: baseCurrency!, toCurrency: toCurrency!, amount: 5)
                    expect(amount).to(equal(15))
                }
            }
        }
    }
}
