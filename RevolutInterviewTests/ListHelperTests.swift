//
//  ListHelperTests.swift
//  RevolutInterviewTests
//
//  Created by Kerem Gunduz on 5.08.2018.
//

import XCTest
import Quick
import Nimble
@testable import RevolutInterview

class ListHelperTests: BaseTest {

    override func spec() {
        describe("ListHelper Class Tests") {
            context("ListHelper functions") {
                var usd: Currency?
                var index: Int?

                beforeEach {
                    let list = getMockList()
                    usd = ListHelper.getCurrency("USD", list: list)
                    index = ListHelper.getCurrencyIndex("EUR", list: list)
                }
                it("getCurrency with code method should return object") {
                    expect(usd).notTo(beNil())
                }
                it("getCurrency with code method should return correnct object") {
                    expect(usd!.code).to(equal("USD"))
                }
                it("getCurrency with code method should return index") {
                    expect(index).notTo(beNil())
                }
                it("getCurrency with code method should return correct index") {
                    expect(index!).to(equal(0))
                }

            }
        }
    }
}
