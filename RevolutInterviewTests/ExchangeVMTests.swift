//
//  RevolutInterviewTests.swift
//  RevolutInterviewTests
//
//  Created by Kerem Gunduz on 4.08.2018.
//

import XCTest
import Quick
import Nimble
@testable import RevolutInterview

class ExchangeVMTests: BaseTest {

    override func spec() {
        describe("ExchangeVM Tests") {
            var viewModel: ExchangeVM!
            let base = "EUR"
            beforeEach {
                viewModel = ExchangeVM(service: ExchangeServiceStub())
                viewModel.fetchRates(base: base)
            }
            context("Base Check") {
                it("Base code should be same as fetchRates parameter") {
                    expect(viewModel.getBaseCode()).to(equal(base))
                }
                it("setBase and check with code") {
                    viewModel.setBase("USD")
                    expect(viewModel.getBaseCode()).to(equal("USD"))
                }
                it("setBase and check with index") {
                    viewModel.setBase(2)
                    expect(viewModel.getBaseCode()).to(equal(getMockList()[2].code))
                }
                it("Base rate should be same as fetchRates parameter") {
                    expect(viewModel.getBaseRate()).to(equal("1.0000"))
                }
            }
            context("Total Count Check") {
                it("getTotalCount should return value exact as list") {
                    expect(viewModel.getTotalCount()).to(equal(getMockList().count))
                }
            }
            context("getCodeAt Check") {
                it("getCodeAt should return value exact as getCodeAt index") {
                    expect(viewModel.getCodeAt(index: 1)).to(equal(getMockList()[1].code))
                }
            }
            context("get Rate Check") {
                it("Rate should return value exact as list") {
                    expect(viewModel.getRate(index: 3)).to(equal(Formatter.formatDouble(getMockList()[3].rate)))
                }
            }

        }
    }
}

class ExchangeServiceStub: ExchangeService {
    override func getRates(base: String, succeed: @escaping (([Currency]) -> Void), failure: @escaping ((NSError) -> Void)) {
        succeed(getMockList())
    }
}
