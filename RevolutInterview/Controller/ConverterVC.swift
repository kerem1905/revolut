//
//  ConverterVC.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

import UIKit

enum Section: Int, EnumCollection {
    case base = 0
    case currency = 1

}
class ConverterVC: BaseVC {

    // MARK: Dispose Bag
    private var disposal = Disposal()
    private var viewModel = ExchangeVM()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        bindUI()
        viewModel.fetchRates(base: "EUR")
        tableView?.dataSource = self
        tableView?.delegate = self
        tableView?.tableFooterView = UIView()
        tableView?.register(UINib(nibName: ConverterCell.kCellIdentifier, bundle: nil), forCellReuseIdentifier: ConverterCell.kCellIdentifier)
    }

    private func bindUI() {
        viewModel.state.observe { [weak self] state in
            switch state {
            case .fetched:
                self?.tableView.reloadData()
            case .amountorRateChanged:
                self?.tableView.reloadSections([Section.currency.rawValue], with: .none)
            case .baseRateChanged(let index):
                let indexPath = IndexPath(row: index, section: Section.currency.rawValue)
                self?.tableView.beginUpdates()
                self?.tableView.moveRow(at: IndexPath(row: 0, section: Section.base.rawValue), to: IndexPath(row: 0, section: Section.currency.rawValue))
                self?.tableView.moveRow(at: indexPath, to: IndexPath(row: 0, section: Section.base.rawValue))
                self?.tableView.endUpdates()
                self?.tableView.reloadRows(at: [IndexPath(row: 0, section: Section.currency.rawValue)], with: .none)
            default: break
            }
            }.add(to: &disposal)
    }
}

extension ConverterVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == Section.base.rawValue {
            return 1
        }
        return viewModel.getTotalCount()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allValues.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ConverterCell.kCellIdentifier, for: indexPath) as! ConverterCell
        cell.delegate = self
        if indexPath.section == Section.base.rawValue {
            cell.setupCell(code: viewModel.getBaseCode(), value: viewModel.getBaseRate())
        } else {
            cell.setupCell(code: viewModel.getCodeAt(index: indexPath.row), value: viewModel.getRate(index: indexPath.row))
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != Section.base.rawValue {
            viewModel.setBase(indexPath.row)
        }
    }

}
extension ConverterVC: ConverterCellProtocol {
    func handleCellSelection(cell: ConverterCell, code: String) {
        viewModel.setBase(code)
    }

    func handleCellAmountChange(cell: ConverterCell, amount: Double) {
        viewModel.setAmount(amount)
    }
}
