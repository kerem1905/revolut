//
//  ExchangeVM.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

import Foundation

enum State {
    case unknown
    case loading
    case fetched
    case amountorRateChanged
    case baseRateChanged(index: Int)
    case error(error: NSError)
}

class ExchangeVM: BaseVM {

    private let queue = DispatchQueue(label: "ExchangeVM")
    private var list: [Currency] = [Currency]()
    private var baseCurrency: Currency = Currency(code: "EUR", rate: 1.0)

    private var service: ExchangeService
    private var currentAmount: Double = 1

    init(service: ExchangeService) {
        self.service = service
    }

    convenience override init() {
        self.init(service: ExchangeService())
    }

    func fetchRates(base: String) {
        setState(.loading)
        if baseCurrency.code != base {
            baseCurrency = list.first { currency -> Bool in currency.code == base}!
        }
        service.getRates(base: base, succeed: { (responseList) in
            var state: State = .fetched
            if self.list.count == 0 {
                self.list = responseList
            } else {
                self.updateRates(responseList)
                state = .amountorRateChanged
            }
            self.setState(state)
            self.scheduleNext()
        }) { (error) in
            self.setState(.error(error: error))
            self.scheduleNext()
        }
    }

    func getCodeAt(index: Int) -> String {
        return list[index].code
    }

    func getRate(index: Int) -> String? {
        let value: Double = UnitHelper.calculateAmount(baseCurrency: baseCurrency, toCurrency: list[index], amount: currentAmount)
        if value == 0.0 {
            return nil
        }
        return Formatter.formatDouble(value)
    }

    func getTotalCount() -> Int {
        return list.count
    }

    func getBaseCode() -> String {
        return baseCurrency.code
    }

    func getBaseRate() -> String {
        return Formatter.formatDouble(baseCurrency.rate)
    }

    func setAmount(_ amount: Double) {
        currentAmount = amount
        setState(.amountorRateChanged)
    }

    func setBase(_ code: String) {
        if code != baseCurrency.code {
            let tempBase = baseCurrency
            if let currency = ListHelper.getCurrency(code, list: self.list) {
                baseCurrency = currency
                baseCurrency.rate = 1
                let indexToRemove = ListHelper.getCurrencyIndex(code, list: self.list)
                if let indexToRemove = indexToRemove {
                    list.remove(at: indexToRemove)
                    list.insert(tempBase, at: 0)
                    setState(.baseRateChanged(index: indexToRemove))
                }
            }

        }
    }

    func setBase(_ index: Int) {
        let currency = list[index]
        if currency.code != baseCurrency.code {
            let tempBase = baseCurrency
            baseCurrency = currency
            baseCurrency.rate = 1
            let indexToRemove = ListHelper.getCurrencyIndex(currency.code, list: self.list)
            if let indexToRemove = indexToRemove {
                list.remove(at: indexToRemove)
                list.insert(tempBase, at: 0)
                setState(.baseRateChanged(index: indexToRemove))
            }
        }
    }

    private func updateRates(_ newList: [Currency]) {
        self.list = list.map { (currency) -> Currency in
            let newCurrency = ListHelper.getCurrency(currency.code, list: newList)
            if let newCurrency = newCurrency {
                return Currency(code: currency.code, rate: newCurrency.rate)
            }
            return Currency()
        }
    }
}

// MARK: Schedule Methods
extension ExchangeVM {
    private func scheduleNext() {
        let workItem = DispatchWorkItem { self.executeFetch() }
        queue.asyncAfter(deadline: .now() + 1, execute: workItem)
    }

    private func executeFetch() {
        self.fetchRates(base: self.baseCurrency.code)
    }
}
