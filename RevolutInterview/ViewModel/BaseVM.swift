//
//  BaseVM.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

import Foundation

class BaseVM {
    internal private(set) var state = Observable(State.unknown)
    func setState(_ state: State) {
        self.state.value = state
    }
}
