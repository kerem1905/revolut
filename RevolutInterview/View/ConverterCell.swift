//
//  ConverterCell.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

import UIKit

protocol ConverterCellProtocol: class {
    func handleCellSelection(cell: ConverterCell, code: String)
    func handleCellAmountChange(cell: ConverterCell, amount: Double)
}

class ConverterCell: UITableViewCell, UITextFieldDelegate {

    static let kCellIdentifier = "ConverterCell"

    weak var delegate: ConverterCellProtocol?
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelDescription: UILabel!
    @IBOutlet private weak var textFieldConverter: ConverterTextField!
    @IBOutlet private weak var imageViewIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldConverter.delegate = self
        selectionStyle = .none
    }

    func setupCell(code: String, value: String?) {
        labelTitle.text = code
        labelDescription.text = Locale.current.localizedString(forCurrencyCode: code)
        textFieldConverter.text = value
        imageViewIcon.image = UIImage(named: code)
    }

    override func prepareForReuse() {
        labelTitle.text = ""
        labelDescription.text = ""
        imageViewIcon.image = nil
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldConverter {
            delegate?.handleCellSelection(cell: self, code: labelTitle.text!)
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldConverter {
            let newValue = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            if textField.text != newValue {
                delegate?.handleCellAmountChange(cell: self, amount: Formatter.parseString(newValue))
            }
        }
        return true
    }

}
