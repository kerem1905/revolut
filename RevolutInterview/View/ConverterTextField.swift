//
//  ConverterTextField.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

import UIKit

class ConverterTextField: UITextField {

    private let borderHeight: CGFloat = 1.0
    private let border = CALayer()

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.borderStyle = .none
        self.keyboardType = .decimalPad
        self.placeholder = "0"
        self.attributedPlaceholder = NSAttributedString(string: "0", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        addBorder()
    }

    private func addBorder() {
        border.backgroundColor = UIColor.gray.cgColor
        border.frame = CGRect(x: 0, y: bounds.size.height - borderHeight, width: bounds.size.width, height: borderHeight)
        self.layer.addSublayer(border)
    }

    override func becomeFirstResponder() -> Bool {
        border.backgroundColor = UIColor.blue.cgColor
        return super.becomeFirstResponder()

    }

    override func resignFirstResponder() -> Bool {
        border.backgroundColor = UIColor.gray.cgColor
        return super.resignFirstResponder()
    }
}
