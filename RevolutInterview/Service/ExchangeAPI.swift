//
//  ExchangeService.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

import Moya

let api = MoyaProvider<ExchangeAPI>()

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

public enum ExchangeAPI {
    case getRates(String)
}

extension ExchangeAPI: TargetType {
    public var baseURL: URL { return URL(string: "https://revolut.duckdns.org")! }
    public var path: String {
        switch self {
        case .getRates:
            return "/latest"
        }
    }
    public var method: Moya.Method {
        return .get
    }
    public var task: Task {
        switch self {
        case .getRates(let base):
            return .requestParameters(parameters: ["base": base], encoding: URLEncoding.default)
        }
    }
    public var validationType: ValidationType {
        return .successCodes
    }

    public var headers: [String: String]? {
        return nil
    }

    public var sampleData: Data {
        return Data()
    }

}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}
