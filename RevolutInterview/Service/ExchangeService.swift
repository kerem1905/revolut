//
//  ExchangeService.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//
import Foundation

class ExchangeService {

    func getRates(base: String, succeed: @escaping (([Currency]) -> Void), failure: @escaping ((NSError) -> Void)) {
        api.request(.getRates(base)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let mainResponse =  try response.map(ExchangeResponse.self)
                    let list = self.mapResponse(mainResponse)
                    succeed(list)
                } catch {
                    failure(error as NSError)
                }
            case .failure(let error):
                failure(NSError.init(domain: error.errorDescription!, code: -1, userInfo: nil))
            }
        }
    }

    func mapResponse(_ response: ExchangeResponse) -> [Currency] {
         var array = response.rates.map { (key, value) -> Currency in
            Currency(code: key, rate: value)
        }
        array.sort { (curr1, curr2) -> Bool in
            return curr1.code < curr2.code
        }
        return array
    }
}
