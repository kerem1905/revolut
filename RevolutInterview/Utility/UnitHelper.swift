//
//  UnitHelper.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 5.08.2018.
//

import Foundation

class UnitHelper {

    static func calculateAmount(baseCurrency: Currency, toCurrency: Currency, amount: Double = 1) -> Double {
        if baseCurrency.rate > 0 {
            return toCurrency.rate * amount / baseCurrency.rate
        }
        return 0
    }
}
