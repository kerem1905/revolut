//
//  Formatter.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 5.08.2018.
//

import Foundation

class Formatter {

    static func formatDouble(_ value: Double) -> String {
        return String.localizedStringWithFormat("%.4f", value)
    }

    static func parseString(_ value: String?) -> Double {
        if let value = value {
            return Double(value) ?? 0
        }
        return 0
    }
}
