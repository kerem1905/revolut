//
//  ListHelper.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 5.08.2018.
//

import Foundation

class ListHelper {

    static func getCurrency(_ code: String, list: [Currency]) -> Currency? {
        return list.first { (currency) -> Bool in
            currency.code == code
            }
    }

    static func getCurrencyIndex(_ code: String, list: [Currency]) -> Int? {
        return list.index(where: { (currency) -> Bool in
            currency.code == code
        })
    }
}
