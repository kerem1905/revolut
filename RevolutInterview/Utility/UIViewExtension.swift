//
//  UIViewExtension.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

import UIKit

protocol UIViewLoading {}

extension UIViewLoading where Self: UIView {

    static func loadFromNib() -> Self {
        let nib = UINib(nibName: String(describing: self), bundle: nil)
        return (nib.instantiate(withOwner: self, options: nil).first as? Self)!
    }

    func lookForSuperviewOfType<T: UIView>(_ type: T.Type) -> T? {
        var viewOrNil: UIView? = self
        while let view = viewOrNil {
            if let lookForSuperview = view as? T {
                return lookForSuperview
            }
            viewOrNil = view.superview
        }
        return nil
    }
}
