//
//  ExchangeResponse.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

struct ExchangeResponse: Decodable {
    let base: String
    let rates: [String: Double]
}
