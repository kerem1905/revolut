//
//  ExchangeItem.swift
//  RevolutInterview
//
//  Created by Kerem Gunduz on 4.08.2018.
//

struct Currency: Equatable, Hashable {
    var rate: Double
    var code: String

    init() {
       self.init(code: "", rate: 0)
    }

    init(code: String, rate: Double) {
        self.code = code
        self.rate = rate
    }

    var hashValue: Int {
        return code.hashValue
    }

    public static func == (lhs: Currency, rhs: Currency) -> Bool {
        return lhs.code == rhs.code
    }

}
